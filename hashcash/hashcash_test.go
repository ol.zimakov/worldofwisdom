package hashcash

import (
	"fmt"
	"testing"
)

func TestCountLeadingZeroesFromRune(t *testing.T) {
	var tests = []struct {
		symb rune
		want int
	}{
		{'1', 3},
		{'3', 2},
		{'5', 1},
		{'A', 0},
	}
	for _, test := range tests {
		name := fmt.Sprintf("case(%c)", test.symb)
		t.Run(name, func(t *testing.T) {
			got := countLeadingZeroesFromRune(test.symb)
			if got != test.want {
				t.Errorf("got %d, want %d", got, test.want)
			}
		})
	}
}

func TestCountLeadingZeroesFromString(t *testing.T) {
	var tests = []struct {
		str string
		want int
	}{
		{"001", 11},
		{"3234GHM", 2},
		{"000A", 12},
		{"", 0},
	}
	for _, test := range tests {
		name := fmt.Sprintf("case(%s)", test.str)
		t.Run(name, func(t *testing.T) {
			got := countLeadingZeroesFromString(test.str)
			if got != test.want {
				t.Errorf("got %d, want %d", got, test.want)
			}
		})
	}
}
