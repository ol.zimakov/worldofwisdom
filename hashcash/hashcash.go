//"Hashcash" is a "proof of work."
//Here we have a 256-bit partial SHA collision against the zero string.
//DIFFICULTY is a number of zeroes we want to see after Sha256 used on prefix+suffix.
//This proves to us that someone took the prefix, and tried about 2**DIFFICULTY(reccomend 20) different suffixes.
//Thus we know that someone has burnt around 2**DIFFICULTY(reccomend 20) CPU cycles on the prefix string.
//All prefixes will be a unique challenge string, so old hashcash cannot be recycled.

package hashcash

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strconv"
)

const (
	DIFFICULTY = 20
	SUFFIX_MAX_SIZE = 30
)

//Count Leading zeroes from bit representation of hex(1-F) number(rune)
func countLeadingZeroesFromRune(ch rune) int {
	if ch == '1' {
		return 3
	} else if ch < '4' {
		return 2
	} else if ch < '6' {
		return 1
	}
	return 0
}

//Count Leading zeroes from bit representation of hex(0-F) number(string)
func countLeadingZeroesFromString(str string) int {
	ans := 0
	idx := 0
	for idx < len(str) && str[idx] == '0' {
		ans += 4
		idx += 1
	}
	if idx < len(str) {
		ans += countLeadingZeroesFromRune(rune(str[idx]))
	}
	return ans
}

//Effective and popular HashFunction(Sha 256) used in blockchain technologies
func generateHashBySha256(data []byte) string {
	hasher := sha256.New()
	hasher.Write(data)
	ans := hex.EncodeToString(hasher.Sum(nil))
	return ans
}

//Function brute-force calculating the suffix
func MakeSuffix(prefix []byte) []byte{
	//It is simpler to iterate throw ints, because with DIFFICULTY 20 we have a guarantee to iterate
	//over approximately 2 ** DIFFICULTY numbers. If we need more difficult PoW(>64) we should replace ints
	// with strings
	var intSuffix int64 = 0
	for {
		//Suffix hex string representation
		suffix := []byte(strconv.FormatInt(intSuffix, 16))

		//For correct []byte comparing len([]byte) must be equal
		fixedSizeSuffix := make([]byte, SUFFIX_MAX_SIZE)
		copy(fixedSizeSuffix[:], suffix)

		//Data if full string for hashing
		data := bytes.Join([][]byte{prefix, fixedSizeSuffix}, []byte(" "))
		hashString := generateHashBySha256(data)
		zeroes := countLeadingZeroesFromString(hashString)

		//Check correctness
		if zeroes == DIFFICULTY {
			return suffix
		}
		intSuffix += 1
	}
}

//Verifying PoW function with Compexity of O(len(suffix+prefix))
//e.t.c. with DIFFICULTY of 20 2**30 faster than calculating suffix
func VerifySuffix(prefix []byte, suffix []byte) bool {
	fmt.Print("(", string(prefix), ") (", string(suffix), ")")
	data := bytes.Join([][]byte{prefix, suffix}, []byte(" "))
	hashString := generateHashBySha256(data)
	return countLeadingZeroesFromString(hashString) == DIFFICULTY
}
