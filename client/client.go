package main

import (
	"bytes"
	"log"
	"net"
	"os"
	"time"
	hc "world_of_wisdom/hashcash"
)

const (
	CONN_HOST = "localhost"
	CONN_PORT = "3333"
	CONN_TYPE = "tcp"
	PREFIX_LEN = 20
	WISDOM_LEN = 1024
)

//Function asking the prefix and brute-force calculating the suffix for further hashing
func askWisdom(conn net.Conn) (string, error) {
	prefix := make([]byte, PREFIX_LEN)
	_, err := conn.Read(prefix)
	if err != nil {
		log.Print("err while getting prefix")
		return "", err
	}
	//PoW timeTracking
	start := time.Now()
	//Calculate suffix
	suffix := hc.MakeSuffix(prefix)
	log.Print("Time suffix calculated: ", time.Since(start).String())
	_, err = conn.Write(suffix)
	if err != nil {
		return "", err
	}

	//Recieving wisdom
	wisdom := make([]byte, WISDOM_LEN)
	_, err = conn.Read(wisdom)
	if err := conn.Close(); err != nil {
		return "", err
	}
	if err != nil {
		log.Print("Error while getting wisdom, PoW failed")
		return "", err
	}
	//trim NULLs
	wisdom = bytes.Trim(wisdom, "\x00")
	//Retutn ans if no errors
	return string(wisdom), nil
}

//Simple tcp server
func main() {
	//logging
	file, err := os.OpenFile("client.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	log.SetOutput(file)

	log.Print("Launching client...")
	defer log.Print("Shutdown client...")
	for {
		conn, err := net.Dial(CONN_TYPE, CONN_HOST+":"+CONN_PORT)
		if err != nil {
			log.Print("Error while connecting:", err.Error())
			os.Exit(1)
		}

		//Trying to connect with server via tcp
		if wisdom, err := askWisdom(conn); err != nil {
			log.Print("No wisdom: ", err.Error())
			return
		} else {
			//print answer and shutdown
			log.Print(wisdom)
			return
		}
	}
}