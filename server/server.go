package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"time"
	hc "world_of_wisdom/hashcash"
)

const (
	CONN_HOST = "localhost"
	CONN_PORT = "3333"
	CONN_TYPE = "tcp"
	PREFIX_LEN = 20
	SUFFIX_MAX_SIZE = 30
	LetterRunes = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	QuoteGeneratorUrl = "https://api.quotable.io/random"
)

//Type for getting quotes
type structResp struct {
	Id           string   `json:"_id"`
	Tags         []string `json:"tags"`
	Content      string   `json:"content"`
	Author       string   `json:"author"`
	AuthorSlug   string   `json:"authorSlug"`
	Length       int      `json:"length"`
	DateAdded    string   `json:"dateAdded"`
	DateModified string   `json:"dateModified"`
}

//getRandomQuote
func getRandomQuote() (string, error) {
	const baseQuote = "quote)"
	resp, err := http.Get(QuoteGeneratorUrl)
	if err != nil {
		return baseQuote, err
	} else {
		data := new(structResp)
		err := json.NewDecoder(resp.Body).Decode(&data)
		if err != nil {
			return baseQuote, err
		}
		return data.Content + " - " + data.Author, nil
	}
}

//Init random seed
func init() {
	rand.Seed(time.Now().UnixNano())
}

//Function generating random string of fixed size
func generatePrefix(n int) string {
	ans := make([]rune, n)
	for i := range ans {
		ans[i] = rune(LetterRunes[rand.Intn(len(LetterRunes))])
	}
	return string(ans)
}

//Function for Check PoW and sending message to client
func handleRequest(conn net.Conn) {
	log.Print("Start connection..")
	defer conn.Close()

	//Prefix of the string from which the hash will be taken
	Prefix := []byte(generatePrefix(PREFIX_LEN))
	conn.Write(Prefix)
	log.Print("PoW request sent")
	//Getting suffix(Proof) of the string from the client
	suffix := make([]byte, SUFFIX_MAX_SIZE)
	_, err := conn.Read(suffix)
	if err != nil {
		fmt.Println("Error reading:", err.Error())
		return
	}
	log.Print("Proof checking..")
	//Prooving timeTracking
	start := time.Now()
	//Verifying work and sending an answer
	ok := hc.VerifySuffix(Prefix, suffix)
	log.Print("Prooving calculated: ", time.Since(start).String())
	if ok {
		quote, err := getRandomQuote()
		if err != nil {
			log.Print("Error with getting quote: ", err.Error())
		}
		conn.Write([]byte(quote))
		log.Print("Wisdom sent")
	} else {
		log.Print("PoW failed")
	}
	return
}

//Simple tcp server
func main() {
	//logging
	file, err := os.OpenFile("server.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	log.SetOutput(file)

	log.Print("Start server...")
	defer log.Print("Shutdown server...")

	l, err := net.Listen(CONN_TYPE, CONN_HOST+":"+CONN_PORT)
	defer func(l net.Listener) {
		err := l.Close()
		if err != nil {
			log.Print("Error closing connection:", err.Error())
		}
	}(l)

	if err != nil {
		log.Print("Error listening:", err.Error())
		os.Exit(1)
	}
	log.Print("Listening on " + CONN_HOST + ":" + CONN_PORT)
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Print("Error accepting: ", err.Error())
			os.Exit(1)
		}
		go handleRequest(conn)
	}
}