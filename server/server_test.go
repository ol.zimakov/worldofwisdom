package main

import (
	"fmt"
	"testing"
)

func TestGetRandomQuote(t *testing.T) {
	_, err := getRandomQuote()
	if err != nil {
		t.Errorf("Quote generator unavailable or parse error")
	}
}

func TestGeneratePrefix(t *testing.T) {
	var tests = []int{1, 2, 3, 4, 5}
	for _, test := range tests {
		name := fmt.Sprintf("case(%d)", test)
		t.Run(name, func(t *testing.T) {
			got := len(generatePrefix(test))
			if got != test {
				t.Errorf("Prefix generated with wrong lengh %d", got)
			}
		})
	}
}
