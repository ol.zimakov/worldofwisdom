# WorldOfWisdom

Here is implementation of simple tcp-client-server app.
Server continuously listening chosen port and, when client connected, 
asked this client for challenge, and, if client passes, sent him a random
quote. When client receives quote or not, he disconnects and shutdowns. 

This challenge is needed for protection from DDOS attacks, here used a 
Proof of Work challenge with challenge-response protocol. As an PoW algo,
here used "HashCash" with sha256 as hash function. "HashCash" was chosen 
as most popular for same task in cryptography. Complexity of Pow can be 
tuned by DIFFICULTY parameter. Usually used DIFFICULTY = 20. With this
setting calculating suffix longs about 1s, proving about 5µs.

All results, timetracking, and conditions translating to server.log and
client.log, which automatically created when server and client run.


# Run
$ docker-compose build

$ docker-compose up -d

# Links
https://en.wikipedia.org/wiki/Proof_of_work

https://en.wikipedia.org/wiki/Hashcash
